/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package core;

import java.util.ArrayList;

/**
 *
 * @author Isjhar-pc
 */
public class Node implements Comparable<Node>
{
    private Cluster myCluster = null;
    private String id = "";
    private ArrayList<Edge> neighbourList = new ArrayList<>();
    
    public Node(String id)
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }

    public void setId(String id) 
    {
        this.id = id;
    }
    
    public void AddNeighbour(Edge neighbour)
    {
        neighbourList.add(neighbour);
    }
    
    public ArrayList<Edge> getNeighbour()
    {
        return neighbourList;
    }

    @Override
    public int compareTo(Node node) 
    {
        if(node.getId().equals(node.getId()))
        {
            return 0;
        }
        else
        {
            return -1;
        }
    }

    @Override
    public String toString() 
    {
        String output = "Node Id : " + id;
        String edge = "";
        for(Edge neighbour : neighbourList)
        {
            edge += "\n \t" + neighbour.toString();
        }
        return output + edge;
    }
    
    public Edge getEdgeByDestination(String nodeId)
    {
        for(Edge neighbour : neighbourList)
        {
            if(neighbour.getDestination().getId().equals(nodeId))
            {
                return neighbour;
            }
        }
        return null;
    }
    
    public double getCallDurationToNode(String nodeId)
    {
        return getEdgeByDestination(nodeId).getCallDuration();
    }
    
    public double getKi()
    {
        double kI = 0;
        for(Edge neighbour : neighbourList)
        {
            kI += neighbour.getCallDuration();
        }
        return kI;
    }
    
    public double getKiIn()
    {
        double KiIn = 0;
        for(Edge neighbour : neighbourList)
        {
            if(myCluster == neighbour.getDestination().getMyCluster())
                KiIn += neighbour.getCallDuration();
        }
        return KiIn;
    }

    public Cluster getMyCluster() {
        return myCluster;
    }

    public void setMyCluster(Cluster myCluster) {
        this.myCluster = myCluster;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package core;


import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import util.Debug;
import util.GraphMatrixWriter;

/**
 *
 * @author Isjhar-pc
 */
public class Graph implements Comparable<Graph>
{
    private String id;
    private HashMap<String, Node> nodes = new HashMap<>();
    private ArrayList<Cluster> clusters = new ArrayList<>();
    
    private final String root = "data/";
    
    
    public Graph() {}
    
    public Graph(String id)
    {
        this.id = id;
    }
    
    /**
     * membuat cluster yang setiap cluster berisikan satu node yang ada pada graph ini
     */
    public void initClusters()
    {
        // hapus cluster lama
        clusters.clear();
        // definisikan cluster baru yang isinya berupa satu node
        for(Map.Entry<String, Node> entry : nodes.entrySet())
        {
           Cluster newCluster = new Cluster();
           newCluster.addMember(entry.getValue());
           clusters.add(newCluster);
        }
    }
    
    /**
     * Fungsi untuk menambah node ke dalam graph
     * @param newNodeId id node yang ingin ditambahkan
     */
    public void addNode(String newNodeId)
    {
        // cek jika node dengan id tersebut bukan string kosong
        if(!newNodeId.isEmpty())
        {
            // cek node dengan id tersebut belum ada, biar tidak duplikat
            if(!nodes.containsKey(newNodeId))
            {
                // insert node pada hash map
                nodes.put(newNodeId, new Node(newNodeId));
            }
        }
    }
    
    /**
     * Fungsi untuk menghubungkan 2 node yang ada dalam graph
     * @param nodeId1 id node 1
     * @param nodeId2 id node 2
     * @param duration durasi telpon
     */
    public void connectNodes(String nodeId1, String nodeId2, double duration)
    {
        // get node 1 dan node 2 dari hashmap
        Node node1 = nodes.get(nodeId1);
        Node node2 = nodes.get(nodeId2);
        
        // cek jika dua duanya ada dalam hash map / ditemukan
        if(node1 != null && node2 != null)
        {
            // cek jika bukan  circle / konek ke diri sendiri
            if(node1 != node2)
            {
                // get edge yang menunjuk ke tujuan, node1 -> node2
                // node2 -> node 1
                Edge edge1 = node1.getEdgeByDestination(nodeId2);
                Edge edge2 = node2.getEdgeByDestination(nodeId1);
                
                // jika kedua node sudah terhubung
                if(edge1 != null && edge2 != null)
                {
                    // tambah durasi
                    edge1.AddCallDuration(duration);
                    edge2.AddCallDuration(duration);
                }
                // jika kedua node belum terhubung
                else if(edge1 == null && edge2 == null)
                {
                    // create new add
                    Edge node1ToNode2 = new Edge();
                    node1ToNode2.setCallDuration(duration);
                    Edge node2ToNode1 = node1ToNode2.duplicate();

                    node1ToNode2.setDestination(node2);
                    node2ToNode1.setDestination(node1);

                    node1.AddNeighbour(node1ToNode2);
                    node2.AddNeighbour(node2ToNode1);
                }
                else
                {
                    Debug.logError("Edge at Node 1 or Node 2 is not found, Connect Failed");
                }
            }
            // jika koneksi berupa circle
            else
            {
                // jika sudah pernah ada koneksi circle sebelumnya
                Edge edge = node1.getEdgeByDestination(nodeId2);
                if(edge != null)
                {
                    // tambah durasi
                    edge.AddCallDuration(duration);
                }
                else
                {
                    // create new add
                    Edge circleEdge = new Edge();
                    circleEdge.setDestination(node2);
                    circleEdge.AddCallDuration(duration);
                    node1.AddNeighbour(circleEdge);
                }
            }
        }
        else
        {
            Debug.logError("Node null");
        }
    }
    
    /**
     * Fungsi untuk menghitung nilai M
     * @return 
     */
    public double getM()
    {
        double m = 0;
        
        // menelusuri node by node menggunakan alogirmat BFS
        // init queue
        Queue<Node> queue = new LinkedList<>();
        ArrayList<Node> visitedList = new ArrayList<>();
        
        // telusuri semua node
        for(Map.Entry<String, Node> entry : nodes.entrySet())
        {
            // jika node yang diakses sekarang belum pernah dikunjungi
            if(!visitedList.contains(entry.getValue()))
            {
                // enqueue node tersebut
                queue.add(entry.getValue());

                // cek jika queue tidak kosong
                while(!queue.isEmpty())
                {
                    // dequeue node yang ada pada queue untuk dijadikan visit node
                    Node visitNode = queue.poll();
                    // tandai visit node sebagai node yang sudah dikunjungi
                    visitedList.add(visitNode);
                    
                    // kunjungi semua tetangga dari visit node
                    for(Edge edge : visitNode.getNeighbour())
                    {
                        // jika node tetangga belum dikunjungi
                        if(!visitedList.contains(edge.getDestination()))
                        {
                            // jika node tetangga belum ada di queue
                            if(!queue.contains(edge.getDestination()))
                                // enqueue node tetangga
                                queue.add(edge.getDestination());
                            // tambah nilai m
                            m += edge.getCallDuration();
                        }
                        else
                        {
                            // jika tetangga yang dikunjungi adalah diri sendiri (circle)
                            if(visitNode == edge.getDestination())
                            {
                                // tambah m
                                m += edge.getCallDuration();
                            }
                        }
                    }
                }
            }
        }
        return m;
    }
   
    public HashMap<String, Node> getNodes()
    {
        return nodes;
    }
    

    @Override
    public String toString() {
        String output = "";
        int counter = 0;
        for(Map.Entry<String, Node> entry : nodes.entrySet())
        {
            if (counter > 0)
                output += "\n " + entry.getValue().toString();
            else
                output += entry.getValue().toString();
            
            counter++;
        }
        return output;
    }
    
    /**
     * Fungsi untuk clustering menggunkan algoritma blondel
     */
    public void clustering()
    {
        // define variable
        Cluster cluster1;
        Cluster cluster2;
        int memberSize;
        Node movedNode;
        Cluster currentClusterMovedNode;
        Cluster maxCluster;
        double maxDeltaQ;
        double m;
        boolean isRepeat;
        int beforeMergeClusterSize; 
        int iteration = 0;
        
        
        do
        {
            Debug.logFormat("Iterasi : %d", iteration);
            iteration++;
            
            // init cluster dan m
            initClusters();
            m = getM();
            
            do
            {
                isRepeat = false;
                maxCluster = null;
                movedNode = null;
                currentClusterMovedNode = null;
                
                // kunjungi semua cluster
                for(int i = 0; i < clusters.size(); i++)
                {
                    // get cluster 1
                    cluster1 = clusters.get(i);
                    memberSize = cluster1.getMembers().size();

                    // kunjungi semua node yang ada pada cluster i
                    for(int j = 0; j < memberSize; j++)
                    {
                        // keluarkan node pada index 0 yang ada pada cluster i
                        movedNode = cluster1.removeMember(0);
                        maxCluster = null;
                        maxDeltaQ = 0;
                        
                        // kunjungi semua cluster
                        for(int k = 0; k < clusters.size(); k++)
                        {
                            // jika cluster k, yaitu cluster yang dikunjungi sekarang, bukan cluster i
                            // (diri sendiri)
                            if(i != k)
                            {
                                cluster2 = clusters.get(k);
                                // jika cluster k tidak kosong
                                if(!cluster2.getMembers().isEmpty())
                                {
                                    // masukkan node yang di remove dari cluster i tadi ke cluster k
                                    cluster2.addMember(movedNode);
                                    // kemudian hitung delta q
                                    double deltaQ = getDeltaQ(cluster2.getSigmaIn(), cluster2.getSigmaTot(), movedNode.getKiIn(), movedNode.getKi(), m);
                                    Debug.logFormat("Move Node : %s, From Cluster : %d, to Cluster : %d, deltaQ : %f", movedNode.getId(), i, k, deltaQ);
                                    // jika delta q lebih besar dari delta q max
                                    if(deltaQ > maxDeltaQ)
                                    {
                                        // tandai delta q sebagai max delata q
                                        maxDeltaQ = deltaQ;
                                        maxCluster = cluster2;
                                        currentClusterMovedNode = cluster1;
                                    }
                                    // kemudian keluarkan kembali node yang dimasukkan tadi dari cluster k
                                    cluster2.removeMember(movedNode);
                                }
                            }
                        }

                        // kembalikan node yang dikeluarkan dari cluster i tadi
                        cluster1.addMember(movedNode);
                    }          
                }

                // jika node cluster yang dikeluarkan dari cluster i tadi harus dipidahkan ke cluster lain
                if(maxCluster != null)
                {
                    // tandai bahwa proses ini harus diulang lagi, karena ada perubahan pada cluster
                    isRepeat = true;
                    // jika node yang dikeluarkan dari cluster i tidak null
                    if(movedNode == null)
                    {
                        Debug.logError("moved Node fail, moved Node null");
                    }
                    else
                    {
                        // pindahkan node yang ada pada cluster i tadi ke cluster yang menjadikan nilai deltaq max
                        currentClusterMovedNode.removeMember(movedNode);
                        maxCluster.addMember(movedNode);
                    }
                }
            }
            // jika proses harus diulang
            while(isRepeat); 
            
            // catat jumlah cluster saat ini, sebelum di merge
            beforeMergeClusterSize = clusters.size();
            
            // merge cluster
            mergeClusters();
            Debug.log("\n");
        }
        // jika jumlah cluster menjadi lebih kecil setelah proses merge, maka ulangi proses
        while(nodes.size() < beforeMergeClusterSize);
        initClusters();
    }
    
    /**
     * Fungsi untuk merge banyak node yang ada pada setiap cluster pada graph ini menjadi 1 node 1 cluster
     */
    public void mergeClusters()
    {
        // init graph dan node baru
        Graph newGraph = new Graph();
        for(Cluster cluster : clusters)
        {
            // tambah kan node baru dengan id gabungan dari id node2 yang menjadi member pada cluster tersebut
            newGraph.addNode(cluster.getId());
        }
        
        // telusuri semua cluster yang ada di graph ini
        for(int i = 0; i < clusters.size(); i++)
        {
            // get cluster i
            Cluster cluster1 = clusters.get(i);
            // telusuri cluster yang ada di list graph start dari index i
            for(int j = i; j < clusters.size(); j++)
            {
                // get cluster j
                Cluster cluster2 = clusters.get(j);
                double totalDurationFromCluster1ToCluser2 = 0;
                
                // jika cluster i dan cluster j bukan cluster yang sama
                if(cluster1 != cluster2)
                {
                    // kunjungin semua node yang ada di graph ini
                    for(Map.Entry<String, Node> entryNode : nodes.entrySet())
                    {
                        // kunjungi tetangga dari setiap node yang sedang dikunjungi
                        for(Edge neighbour : entryNode.getValue().getNeighbour())
                        {
                            // jika cluster dari node yang sedang dikunjungi adalah cluster i dan
                            // jika cluster dari node tetangga yang sedang dikunjungi adalah cluster j maka
                            // ditemukan bahwa ada koneksi yang terbentuk antara cluster i dan j
                            if(entryNode.getValue().getMyCluster() == cluster1 && 
                                    neighbour.getDestination().getMyCluster() == cluster2)
                            {
                                // catat durasi yang dibentuk oleh cluster i dan j
                                totalDurationFromCluster1ToCluser2 += neighbour.getCallDuration();
                            }
                        }
                    }
                }
                // jika cluster i dan j cluster yang sama
                else
                {
                    // total durasi yang dibentuk antara cluster i dan j adalah nilai dari sigma in
                    totalDurationFromCluster1ToCluser2 = cluster1.getSigmaIn();
                }
                
                // jika ditemukan adanya hubungan antara cluster i dan j
                if(totalDurationFromCluster1ToCluser2 > 0)
                {
                    // init id cluster yang di merge tadi
                    String cluster1Id = cluster1.getId();
                    String cluster2Id = cluster2.getId();
                    // ubah cluster i dan j tadi menjadi node dan hubungkan mereka
                    newGraph.addNode(cluster1Id);
                    newGraph.addNode(cluster2Id);
                    newGraph.connectNodes(cluster1Id, cluster2Id, totalDurationFromCluster1ToCluser2);
                }
            }
        }
        
        // hapus node node lama
        nodes.clear();
        // assign node2 baru hasil merge cluster
        nodes = newGraph.getNodes();
        // hapus cluster lama 
        clusters.clear();
    }
    
    /**
     * Fungsi untuk menghitung jumlah cluster yang ada pada graph ini
     * @return 
     */
    public int getClusterSize()
    {
        int clusterSize = 0;
        for(Cluster cluster : clusters)
        {
            if(!cluster.getMembers().isEmpty())
                clusterSize++;
        }
        return clusterSize;
    }
    
    /**
     * Fungsi untuk menghitung deltaQ
     * @param sigmaIn
     * @param sigmaTot
     * @param kiIn
     * @param ki
     * @param m
     * @return 
     */
    public static double getDeltaQ(double sigmaIn, double sigmaTot, double kiIn, double ki, double m)
    {
        double deltaQ = (((sigmaIn + kiIn) / (2 * m)) - ((sigmaTot + ki) / (2 * m))) - ((sigmaIn / (2 * m)) - Math.pow(sigmaTot / (2 * m), 2) - Math.pow(ki / (2 * m), 2));
//        Debug.logFormat("delta Q = %f, sigmaIn = %f, sigmaTot = %f, kiIn = %f, ki = %f, m = %f", deltaQ, sigmaIn, sigmaTot, kiIn, ki, m);
        return deltaQ;
    }
    
    /**
     * Fungsi untuk menulis graph ini ke file excel dalam bentuk matrix
     * @throws FileNotFoundException
     * @throws IOException 
     */
    public void SaveToFile() throws FileNotFoundException, IOException
    {
        String[][] matrix = new String[nodes.size() + 1][nodes.size() + 1];
        
        // init array list
        ArrayList<Node> tempNodes = new ArrayList<>();
        for(Map.Entry<String, Node> entry : nodes.entrySet())
        {
            tempNodes.add(entry.getValue());
        }
        
        
        // init cell value
        for(int x = 0; x < matrix.length; x++)
            for(int y = 0; y < matrix[x].length; y++)
                matrix[x][y] = "0";
        
        // init column and row identity
        matrix[0][0] = "-";
        for(int i = 0; i < tempNodes.size(); i++)
        {
            matrix[0][i + 1] = matrix[i + 1][0] = tempNodes.get(i).getId();
        }
        
        
        
        // fill matrix
        Node tempNode;
        int neighbourIndex;
        DecimalFormat df = new DecimalFormat("#.##");
        df.setRoundingMode(RoundingMode.CEILING);
        for(int i = 0; i < tempNodes.size(); i++)
        {
            tempNode = tempNodes.get(i);
            for(Edge neighbour : tempNode.getNeighbour())
            {
                neighbourIndex = tempNodes.indexOf(neighbour.getDestination());
                matrix[i + 1][neighbourIndex + 1] = df.format(neighbour.getCallDuration());
            }
        }
        
        Debug.log("\n");
        
        // save file
        GraphMatrixWriter.WriteMatrixToExcel(root + id + ".xlsx", matrix);
    }

    public ArrayList<Cluster> getClusters() {
        return clusters;
    }

    @Override
    public int compareTo(Graph other) 
    {
        return id.compareTo(other.getId());
    }

    public String getId() {
        return id;
    }
}

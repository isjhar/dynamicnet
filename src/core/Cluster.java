/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package core;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

/**
 *
 * @author Isjhar-pc
 */
public class Cluster 
{
    private String id = null;
    private ArrayList<Node> members = new ArrayList<>();
    
    /**
     * Fungsi untuk menambah node yang menjadi bagian cluster ini
     * @param newMember 
     */
    public void addMember(Node newMember)
    {
        // set cluster membaru baru menjadi cluster ini
        newMember.setMyCluster(this);
        // tambah member baru ke dalam list
        members.add(newMember);
    }
    
    /**
     * Fungsi untuk menghitung SigmaIn
     * @return nilai sigma in cluster ini
     */
    public double getSigmaIn()
    {
        double sigmaIn = 0;
        
        // menelusuri node by node, menggunakan algoritma BFS
        // init queue BFS
        Queue<Node> queue = new LinkedList<>();
        ArrayList<Node> visitedList = new ArrayList<>();
        
        // telusuri semua member yang ada di dalam list
        for(Node member : members)
        {
            // cek jika member yang sekarang diakses belum dikunjungi
            if(!visitedList.contains(member))
            {
                // masukkan ke queue
                queue.add(member);

                // cek apakah queue tidak kosong
                while(!queue.isEmpty())
                {
                    // dequeue
                    Node visitNode = queue.poll();
                    // tandai visit node sebagai node yang sudah dikunjungi
                    visitedList.add(visitNode);
                    
                    // telusuri semua edge yang ada pada visit node
                    for(Edge edge : visitNode.getNeighbour())
                    {
                        // cek jika edge tersebut adalah circle, menunjuk dirinya sendiri
                        if(visitNode == edge.getDestination())
                        {
                            // tambah sigma in sesuai durasi pada edge
                            sigmaIn += edge.getCallDuration();
                        }
                        else
                        {
                            // cek jika node tetangga belum dikunjungi dan node tersebut adalah member cluster 
                            // visit node
                            if(!visitedList.contains(edge.getDestination()) && members.contains(edge.getDestination()))
                            {
                                // tambah sigma in sesuai durasi pada edge
                                sigmaIn += edge.getCallDuration();
                                // cek apakah node tetangga ada di dalam queue atau tidak
                                if(!queue.contains(edge.getDestination())) 
                                    queue.add(edge.getDestination());
                            }
                        }
                    }
                }
            }
        }
        return sigmaIn;
    }
    
    /**
     * Fungsi untuk menghitung Sigma Total
     * @return nilai sigma total cluster ini
     */
    public double getSigmaTot()
    {
        double sigmaTot = 0;
        
        // menelusuri node by node, menggunakan algoritma BFS
        // init queue
        Queue<Node> queue = new LinkedList<>();
        ArrayList<Node> visitedList = new ArrayList<>();
        
        // telusuri semua member yang ada di dalam list
        for(Node member : members)
        {
            // cek jika member yang sekarang diakses belum dikunjungi
            if(!visitedList.contains(member))
            {
                // masukkan ke queue
                queue.add(member);

                // cek apakah queue tidak kosong
                while(!queue.isEmpty())
                {
                    // dequeue
                    Node visitNode = queue.poll();
                    // tandai visit node sebagai node yang sudah dikunjungi
                    visitedList.add(visitNode);
                    
                   // telusuri semua edge yang ada pada visit node
                    for(Edge edge : visitNode.getNeighbour())
                    {
                        // cek jika node tetangga dari visit node belum pernah dikunjungi
                        if(!visitedList.contains(edge.getDestination()))
                        {
                            // tambah nilai sigma total berdasarkan durasi pada edge
                            sigmaTot += edge.getCallDuration();
                            
                            // cek jika node tetangga dari visit node adalah member cluster ini dan
                            // tidak ada dalam queue 
                            if(members.contains(edge.getDestination()) && !queue.contains(edge.getDestination()))
                            {
                                // masukkan node tetangga tersebut ke dalam queue
                                queue.add(edge.getDestination());
                            }
                        }
                        else
                        {
                            // cek circle
                            if(visitNode == edge.getDestination())
                            {
                                // tambah nilai sgima total berdasarkan durasi pada edge
                                sigmaTot += edge.getCallDuration();
                            }
                        }
                    }
                }
            }
        }
        return sigmaTot;
    }
    
    /**
     * Fungsi untuk menghapus member cluster berdasarkan indexnya dalam list
     * @param index
     * @return Node yang dihapus
     */
    public Node removeMember(int index)
    {
        // get member pada index 
        Node removedNode = members.get(index);
        // hapus member dari dalam list
        members.remove(index);
        
        // cek jika member tidak null
        if(removedNode != null)
            // set status node tersebut tidak memiliki cluster
            removedNode.setMyCluster(null);
        return removedNode;
    }
    
    /**
     * Fungsi untuk menghafpus cluster berdasarkan reference node
     * @param node 
     */
    public void removeMember(Node node)
    {
        // menghapus node dari list
        members.remove(node);
        // set status node tersebut tidak memiliki cluster
        node.setMyCluster(null);
    }

    public ArrayList<Node> getMembers() {
        return members;
    }
    
    /**
     * Fungsi untuk mendapat id cluster ini. id dibentuk dari gabungan id setiap node yang
     * menjadi member ini, yang dipisahkan dengan tanda ";"
     * @return id cluster ini
     */
    public String getId()
    {
        if(id == null)
        {
            id = "";
            int counter = 0;
            for(Node member : members)
            {
                if(counter == 0)
                    id += member.getId();
                else
                    id += ";" + member.getId();
                counter++;
            }    
        }
        return id;
    }
}

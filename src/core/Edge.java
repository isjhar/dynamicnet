/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package core;


/**
 *
 * @author Isjhar-pc
 */
public class Edge implements Comparable<Edge>
{
    private Node destination = null;
    private double callDuration = 0;
    

    public Node getDestination() 
    {
        return destination;
    }

    public void setDestination(Node destination) 
    {
        this.destination = destination;
    }

    public double getCallDuration() 
    {
        return callDuration;
    }

    public void setCallDuration(double callDuration) 
    {
        this.callDuration = callDuration;
    }
    
    public void AddCallDuration(double additionalCallDuration)
    {
        this.callDuration += additionalCallDuration;
    }

    @Override
    public int compareTo(Edge edge) 
    {
        return this.destination.compareTo(edge.getDestination());
    }
    
    /**
     * Fungsi untuk menduplicate edge ini menjadi object edge baru
     * @return object edge baru dengan property yang sama dengan object ini
     */
    public Edge duplicate()
    {
        Edge cloned = new Edge();
        cloned.setDestination(this.destination);
        cloned.setCallDuration(this.callDuration);
        return cloned;
    }

    @Override
    public String toString() 
    {
        return String.format("Edge:{Destination:%s, Call Duration:[%f]}", destination.getId(), callDuration);
    }



}

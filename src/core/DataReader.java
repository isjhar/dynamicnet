package core;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import util.Debug;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Isjhar-pc
 */
public class DataReader 
{
    /**
     * Fungsi untuk membaca file excel yang menjadi data set kemudian ditransformasikan menjadi graph
     * @param fileName nama file excel data set
     * @return list graph yang ada pada excel tersebut
     * @throws FileNotFoundException
     * @throws IOException 
     */
    public static ArrayList<Graph> readData(String fileName) throws FileNotFoundException, IOException
    {
        // init hasp map
        HashMap<String, Graph> tempResult = new HashMap<>();
        
        // baca file excel
        FileInputStream file = new FileInputStream(fileName);
        // membuat object excel reader
        XSSFWorkbook workbook = new XSSFWorkbook(file);
        // akses worksheet 0
        XSSFSheet worksheet = workbook.getSheetAt(0);
        
        // Create Node Start Here
        // init variable
        int counter = 0;
        int tahun;
        int bulan;
        String graphKey = "";
        String dateString = "";
        String noTelpPemanggil = "";
        String noTelpTujuan = "";
        double duration = 0;
        Iterator<Row> rowIterator = worksheet.iterator();
        
        
        // lompati row 0
        rowIterator.next();
        // cek jika row masih ada
        while(rowIterator.hasNext())
        {
            // akses row berikutnya
            Row row = rowIterator.next();
            // iterate cell
            Iterator<Cell> cellIterator = row.iterator();
            // cek jika cell pada row tersebut masih ada
            while(cellIterator.hasNext())
            {
                // akses cell tersebut
                Cell cell = cellIterator.next();
                // cek nomor kolom
                switch(cell.getColumnIndex())
                {
                    case 0 :
                        // set nilai pada cell ini sebagai no telp pemanggil
                        noTelpPemanggil = cell.getStringCellValue();   
                        break;
                    case 1 : 
                        // set nilai pada cell ini sebagai no telp tujuan
                        noTelpTujuan = cell.getStringCellValue();
                        break;
                    case 2 :
                        // set nilai pada cell ini sebagai durasi menelpon
                        duration = new Double(cell.getNumericCellValue());
                        break;
                    case 3 : 
                        // cek tipe data pada cell ini
                        switch(cell.getCellType())
                        {
                            // kalau tipe tanggal
                            case Cell.CELL_TYPE_NUMERIC:
                                // get tahun dan bulan
                                tahun = cell.getDateCellValue().getYear() + 1900;
                                bulan = cell.getDateCellValue().getMonth() + 1;
                                
                                // set graph key
                                // jika bulan kurang dari 10
                                if(bulan < 10)
                                {
                                    // tambahkan angka 0 di depan bulan tersebut
                                    graphKey = tahun + "-0" + bulan;
                                }   
                                else
                                {
                                    graphKey = tahun + "-" + bulan;
                                }
                                
                                
                                // cek jika graph key belum ada dalam hash map
                                if(!tempResult.containsKey(graphKey))
                                {
                                    // buat graph baru dengan graph key sesuai tanggal cell yg sedang diakses
                                    Graph newGraph = new Graph(graphKey);
                                    // tambah node no telp pemanggil dan no telp tujuan
                                    newGraph.addNode(noTelpPemanggil);
                                    newGraph.addNode(noTelpTujuan);
                                    // hubungkan no telp pemanggil dengan no telp tujuan, dan set durasi menelponya
                                    newGraph.connectNodes(noTelpPemanggil, noTelpTujuan, duration);
                                    tempResult.put(graphKey, newGraph);
                                }
                                else
                                {
                                    // get graph dengan key graph key dari hash map
                                    Graph graph = tempResult.get(graphKey);
                                    // tambah node no telp pemanggil dan no telp tujuan
                                    graph.addNode(noTelpPemanggil);
                                    graph.addNode(noTelpTujuan);
                                    // hubungkan no telp pemanggil dengan no telp tujuan, dan set durasi menelponya
                                    graph.connectNodes(noTelpPemanggil, noTelpTujuan, duration);
                                }
                                    
                                break;
                        }
                        break;
                }
            }           
        }
        
        // konversi hashmap ke array list
        ArrayList<Graph> result = new ArrayList<>();
        for(Map.Entry<String, Graph> entry : tempResult.entrySet())
        {
            Graph graph = entry.getValue();
            result.add(graph);
            Debug.logFormat("Graph With Key : %s, Containts : %d node", entry.getKey(), graph.getNodes().size());
        }
        Debug.log("\n");
        return result;
    }
}

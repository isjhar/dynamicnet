/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFChartSheet;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author Isjhar-pc
 */
public class GraphMatrixWriter 
{
    public static void WriteMatrixToTxt(String pathFile, String[][] matrix) throws IOException
    {
        // output matrix && write data
        String output = "";
        for(int y = 0; y < matrix.length; y++)
        {
            for(int x = 0; x < matrix[y].length; x++)
            {
                if(x == 0)
                {
                    output += matrix[x][y] + "";
                }
                else
                {
                    output += "," + matrix[x][y];
                }
            }
            output += "\n";
        }
        
        Debug.log(output);
       
        FileWriter writer = new FileWriter(pathFile);
        writer.write(output);
        writer.flush();
        writer.close();
    }
    
    public static void WriteMatrixToExcel(String pathFile, String[][] matrix) throws IOException
    {
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet();
        
        // output matrix && write data
        String output = "";
        for(int y = 0; y < matrix.length; y++)
        {
            XSSFRow row = sheet.createRow(y);
            for(int x = 0; x < matrix[y].length; x++)
            {
                XSSFCell cell = row.createCell(x);
                cell.setCellValue(matrix[x][y]);    
            }
        }
        
        Debug.log(output);
       
        FileOutputStream writer = new FileOutputStream(pathFile);
        workbook.write(writer);
        writer.flush();
        writer.close();
    }
}

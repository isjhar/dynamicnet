/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

/**
 *
 * @author Isjhar-pc
 */
public class Debug 
{
    public static void log(String message)
    {
        System.out.println(message);
    }
    
    public static void logFormat(String message, Object... param)
    {
        System.out.println(String.format(message, param));
    }
    
    public static void logError(String message)
    {
        System.err.println(message);
    }
    
    public static void logErrorFormat(String message, Object... param)    
    {
        System.err.println(String.format(message, param));
    }
}

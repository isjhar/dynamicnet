/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package main;


import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import util.Debug;
import core.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Isjhar-pc
 */
public class DynamicNet 
{ 
    
    public static void main(String[] args)
    {
        DynamicNet net = new DynamicNet();
        try {
            String pathFile = "data/DataDummy.xlsx";
            ArrayList<Graph> graphs = DataReader.readData(pathFile);
            ArrayList<ArrayList<Cluster>> lifeTimes = net.dynamicNet(graphs, 0.5, true);
            
            for(ArrayList<Cluster> lifeTime : lifeTimes)
            {
                int i = 0;
                String temp = "";
                for(Cluster cluster : lifeTime)
                {
                    if(i == 0)
                    {
                        temp += "[" + cluster.getId() + "] ";
                    }
                    else
                    {
                        temp += "-> [" + cluster.getId() + "]";
                    }
                    i++;
                }
                Debug.logFormat("Life Time : %s", temp);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(DynamicNet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DynamicNet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Fungsi untuk mencari apakah community T masih ada di list community T+1 atau tidak
     * @param communityT community T (gt)
     * @param communitiesTList list community T (Ct)
     * @param communitiesTPlus1List list community T+1 (Ct+1)
     * @param threshold threshold kemiripan untuk proses match
     * @return null, jika sudah tidak ada, dan sebaliknya
     */
    public Cluster survives(Cluster communityT, ArrayList<Cluster> communitiesTList, ArrayList<Cluster> communitiesTPlus1List, double threshold)
    {
        // baris 1
        Cluster result = match(communityT, communitiesTPlus1List, threshold);
        // baris 2
        for(Cluster community : communitiesTList)
        {
            if(community != communityT)
            {
                // baris 3
                if(result != null && result == match(community, communitiesTPlus1List, threshold))
                {
                    // baris 4
                    return null;
                }
            }
        }
        // baris 7
        return result;
    }
    
    /**
     * fungsi untuk mencari apakah communityT mengalami absorbed pada T+1,
     * @param communityT community T (gt)
     * @param communityTList list community T (Ct)
     * @param communityTPlus1List list community T+1 (Ct+1)
     * @param threshold threshold kemiripan untuk proses match
     * @return Cluster yang me-absorb communit T (gt)
     */
    public Cluster absorbed(Cluster communityT, ArrayList<Cluster> communityTList, ArrayList<Cluster> communityTPlus1List, double threshold)
    {
        // baris 1, cari community yg match dengan community T di list community T+1
        Cluster result = match(communityT, communityTPlus1List, threshold);
        
        // baris 2, telusuri semua community di list community T kecuali community T yang didefinisikan
        // diparameter fungsi
        for(Cluster community : communityTList)
        {
            // kecuali community T
            if(community != communityT)
            {
                // baris 3
                if(result != null && result == match(community, communityTPlus1List, threshold))
                {
                    // baris 4
                    return result;
                }
            }
        }
        // baris 5
        return null;
    }
    
    /**
     * Fungsi untuk mencari pecahan dari community T yang ada pada community T + 1
     * @param communityT community T (gt)
     * @param communityTPlus1List list community T+1 (Ct+1)
     * @param threshold threshold kemiripan untuk proses match
     * @return daftar Cluster yang menjadi pecahan dari community T yang ada pada list community T+1
     */
    public ArrayList<Cluster> split(Cluster communityT, ArrayList<Cluster> communityTPlus1List, double threshold)
    {
        // baris 1
        int m = communityT.getMembers().size();
        int n = 0;
        
        // baris 2, ini splt
        ArrayList<Cluster> splitClusterList = new ArrayList<>();
        String[] nodeIdListCommunityT = communityT.getId().split(";");
        
        // baris 3, hitung n
        for(Cluster communityPlus1 : communityTPlus1List)
        {
            // baris 4, cek ada irisan node antara 2 community
            if(countUnion(nodeIdListCommunityT, communityPlus1.getId().split(";")) > 0)
            {
                // baris 5
                n++;
            }
        }
        
        // baris 8
        // cari community yang telah di split
        for(Cluster communityPlus1 : communityTPlus1List)
        {
            // baris 9
            // cek apakah jumlah irisan lebih besar dari m / n
            double union = countUnion(nodeIdListCommunityT, communityPlus1.getId().split(";"));
            if(union >= (double) m / n)
            {
                // baris 10, tandai sebagai community yang menjadi hasil split
                splitClusterList.add(communityPlus1);
            }
        }
        
        // baris 13, cek match
        Cluster matchResult = match(communityT, splitClusterList, threshold);
        if(matchResult != null)
        {
            return splitClusterList;
        }
        else
        {
            return null;
        }
    }
    
    @Deprecated
    private boolean disappears(Cluster communityT, ArrayList<Cluster> communitiesTList, ArrayList<Cluster> communitiesTPlus1List, double threshold)
    {
        if(survives(communityT, communitiesTList, communitiesTPlus1List, threshold) == null 
                && absorbed(communityT, communitiesTList, communitiesTPlus1List, threshold) == null
                && split(communityT, communitiesTPlus1List, threshold) == null)
        {
            return true;
        }
        return false;
    }
    
    @Deprecated
    private boolean appears(Cluster communityTPlus1, ArrayList<Cluster> communityTList, ArrayList<Cluster> communityTPlus1List, double threshold)
    {
        for(Cluster communityT : communityTList)
        {
            if(communityTPlus1 != survives(communityT, communityTList, communityTPlus1List, threshold) 
                    && communityTPlus1 != absorbed(communityT, communityTList, communityTPlus1List, threshold)
                    && !isSubset(communityT, split(communityT, communityTPlus1List, threshold)))
            {
                return true;
            }
        }
        return false;
    }
    
    @Deprecated
    private static boolean isSubset(Cluster community, ArrayList<Cluster> communitiyList)
    {
        for(Cluster community2 : communitiyList)
        {
            if(community == community2)
            {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Fungsi untuk menghitung jumlah node yang beririsan pada 2 community
     * @param nodeIdList1
     * @param nodeIdList2
     * @return jumlah union
     */
    private static int countUnion(String[] nodeIdList1, String[] nodeIdList2)
    {
        int result = 0;
        // mencari node yang beririsan
        for(String nodeT : nodeIdList1)
        {
            for(String nodeTPlus1 : nodeIdList2)
            {
                if(nodeT.equals(nodeTPlus1))
                {
                    // increment counter
                    result++;
                    break;
                }
            }
        }
        return result;
    }
    
    /**
     * Fungsi menghitung nilai overlap antar dua buah cluster
     * @param cT
     * @param cTPlus1
     * @return nilai overlap
     */
    public double overlap(Cluster cT, Cluster cTPlus1)
    {
        // mencari list id node yang ada pada cluster T dan T+1 dengan mengextract dari id nya
        String[] graphListInClusterT = cT.getId().split(";");
        String[] graphListInClusterTPlus1 = cTPlus1.getId().split(";");
        
        double unionAmount = countUnion(graphListInClusterT, graphListInClusterTPlus1);
        return unionAmount / graphListInClusterT.length;
    }
    
    /**
     * Fungsi untuk mencari community yang mirip dengan community T di list community T+1
     * @param community community yang akan dicari matchnya (gt)
     * @param communitiyTPlus1List list community yang menjadi ruang pencarian (Ct+1)
     * @param threshold threshold kemiripan untuk proses match
     * @return Cluster yang match dengan community T yang ada di community T + 1
     */
    public Cluster match(Cluster community, ArrayList<Cluster> communitiyTPlus1List, double threshold)
    {
        if(!communitiyTPlus1List.isEmpty())
        {
            // baris 1, init overlap dan init candidate match
            Cluster candidate = communitiyTPlus1List.get(0);
            double maxOverlap = overlap(community, candidate); 
            Cluster communityPlus1;

            // baris 2
            for(int i = 1; i < communitiyTPlus1List.size(); i++)
            {
                // baris 3
                communityPlus1 = communitiyTPlus1List.get(i);
                double currentOverlap = overlap(community, communityPlus1);

                // baris 4, cek overlap community i lebih besar daripada max overlap
                if(currentOverlap > maxOverlap)
                {
                    // tandai community i sebagai candidate match
                    // baris 5
                    maxOverlap = currentOverlap;
                    // baris 6
                    candidate = communityPlus1;
                }
            }

            // baris 9, overlap yang jadi candidate memiliki overlap yang lebih besar dari threshold
            // dan cek apakah community yang memiliki nmi terbesaar ada candidate tersebut
            Cluster maxNMI = sn(community, communitiyTPlus1List);
            if(maxOverlap > threshold && maxNMI == candidate)
            {
                return candidate;
            }
            else    
            {
                return null;
            }
        }
        else
        {
            return null;
        }
    }
    
    /**
     * Fungsi untuk mencari Cluster yang memiliki nilai NMI terbesar yang menyatakan 
     * Cluster yang memiliki kedekatan struktur dengan Cluster community T pada list community T+1
     * @param communityT community yang akan dicari kemiripan strukturnya
     * @param communityTPlus1List list community T+1, yang menjadi ruang pencarian
     * @return Cluster yang memiliki kedepakatan struktur paling mirip dengan communiy T
     * yang ada pada list community T+1
     */
    private Cluster sn(Cluster communityT, ArrayList<Cluster> communityTPlus1List)
    {
        Cluster maxNMICluster = null;
        // extract node2 yang dimiliki oleh community T
        String[] nodeListA = communityT.getId().split(";");
        ArrayList<String[]> communityListA = new ArrayList<>();
        communityListA.add(nodeListA);
        double maxNMIValue = 0;
        for(Cluster communityTPlus1 : communityTPlus1List)
        {
            // extract node2 yang ada di cluster tersebut
            String[] nodeListB = communityTPlus1.getId().split(";");
            ArrayList<String[]> communityListB = new ArrayList<>();
            communityListB.add(nodeListB);
            
            // jika maxNMICluster belum di inisialisasi
            if(maxNMICluster == null)
            {
                // tandai community T+1 sebagai max NMI
                maxNMICluster = communityTPlus1;
                maxNMIValue = NMI(communityListA, communityListB);
            }
            else
            {
                
                double newNMIValue = NMI(communityListA, communityListB);
                // cek apakah nilai NMI yang baru lebih besar dari pada nilai NMI max saat ini
                if(newNMIValue >= maxNMIValue)
                {
                    // cluster yang memiliki nilai NMI lebih besar dari 
                    // cluster yang menjadi max NMI sebelumnya ditemukan
                    // tandai cluster tersebut sebagai cluster max NMI
                    maxNMICluster = communityTPlus1;
                    maxNMIValue = newNMIValue;
                }
            }
        }
        return maxNMICluster;
    }
    
    /**
     * Fungsi dynamic net, untuk mencari life time sebuah cluster
     * @param graphList kumpulan graph
     * @param threshold threshold kemiripan untuk proses match
     * @param doCluster parameter untuk mengecek apakah proses clustering mau dilakukan di dalam 
     * method ini atau tidak
     * @return Kumpulan lifetime yang dialami cluster-cluster yang ada pada graphList
     */
    public ArrayList<ArrayList<Cluster>> dynamicNet(ArrayList<Graph> graphList, double threshold, boolean doClustering) throws FileNotFoundException, IOException
    {
        ArrayList<ArrayList<Cluster>> lifeTimes = new ArrayList<>();

        // sorting node pada graph berdasarkan waktunya (tanggal)
        Collections.sort(graphList);
        
        // line 1
        // clustering life time 0 and init lifetime
        if(doClustering)
            graphList.get(0).clustering();
       
        // line  2
        // init life time
        for(Cluster cluster : graphList.get(0).getClusters())
        {
            ArrayList<Cluster> lifeTime = new ArrayList<>();
            lifeTime.add(cluster);
            lifeTimes.add(lifeTime);
        }
        
        // line 3
        for(int i = 1; i < graphList.size(); i++)
        {
            // line 4, extract cluster
            if(doClustering)
                graphList.get(i).clustering();
            
            ArrayList<Cluster> communityTList = graphList.get(i).getClusters();
            ArrayList<Cluster> communityTMin1List = graphList.get(i - 1).getClusters();
            
            // line 5
            // menelusuri cluster pada t-1
            int lifeTimesSize = lifeTimes.size();
            int j = 0;
            while(j < lifeTimesSize)
            {
                // get tail
                Cluster communityTMin1 = lifeTimes.get(j).get(lifeTimes.get(j).size() - 1);
                Cluster communityTIK;
                ArrayList<Cluster> S;
                
                // line 7
                if((communityTIK = survives(communityTMin1, communityTMin1List, communityTList, threshold)) != null
                        || (communityTIK = absorbed(communityTMin1, communityTMin1List, communityTList, threshold)) != null)
                {
                    // line 8, expand life time dengan nambah community TIK
                    lifeTimes.get(j).add(communityTIK);
                    // increment j, akses lifetime berikutnya
                    j++;
                }
                // line 9, cek split
                else if((S = split(communityTMin1, communityTList, threshold)) != null)
                {
                    ArrayList<Cluster> lifeTimeWillDuplicate = lifeTimes.get(j);
                    
                    // line 10, duplicate life time
                    // remove life time yang lama untuk digantikan dengan yang baru, yang telah di 
                    // expand dengan hasil splitnya
                    lifeTimes.remove(j);
                    lifeTimesSize--;
                    for(Cluster splitResult : S)
                    {
                        // membuat lifetime baru, lalu tambahkan ke list
                        ArrayList<Cluster> newLifeTime = new ArrayList<>(lifeTimeWillDuplicate);
                        newLifeTime.add(splitResult);
                        lifeTimes.add(newLifeTime);
                    }
                }
                // line 13
                else
                {
                    // increment j, untuk akses lifetime berikutnya karena lifetime sudah tidak 
                    // extendible
                    j++;
                }
            }
            
            // line 17, cek apakah communityT tidak menjadi tail disalah satu lifetimes, 
            // jika ya buat lifetime baru, karena dia community yang baru muncul
            for(Cluster communityT : communityTList)
            {
                // cari communityT di tail setiap lifetime yang sudah didefinisikan
                boolean isFound = false;
                for(ArrayList<Cluster> lifetime : lifeTimes)
                {
                    // cek jika community ditemukan
                    if(lifetime.get(lifetime.size() - 1) == communityT)
                    {
                        isFound = true;
                        break;
                    }
                }
                
                // jika community tidak ditemukan
                if(!isFound)
                {
                    // buat lifetime baru
                    ArrayList<Cluster> newLifeTime = new ArrayList<>();
                    newLifeTime.add(communityT);
                    lifeTimes.add(newLifeTime);
                }
            }
        }
        return lifeTimes;
    }
    
    /**
     * Fungsi untuk menghitung nilai NMI antara 2 list community
     * @param communityListA
     * @param communityListB
     * @return nilai NMI
     */
    public double NMI(ArrayList<String[]> communityListA, ArrayList<String[]> communityListB)
    {
        // buat matrix dengan ukuran A x B
        double[][] matrixAB = new double[communityListA.size()][communityListB.size()];
        double[] columnCounter = new double[communityListA.size()];
        double[] rowCounter = new double[communityListB.size()];
        double n = 0;
        
        // init matrix
        for(int x = 0; x < matrixAB.length; x++)
        {
            String[] communityA = communityListA.get(x);
            
            // hitung total node di community A
            n += communityA.length;
            
            // telusuri community B
            for(int y = 0; y < matrixAB[x].length; y++)
            {
                int subsetCount = 0;
                String[] communityB = communityListB.get(y);
                
                // hitung total node di community B
                if(x == 0)
                    n += communityB.length;
                
                // hitung jumlah node yang ada di community A dan ada juga di community B
                for(String nodeA : communityA)
                {
                    for(String nodeB : communityB)
                    {
                        if(nodeA.equals(nodeB))
                        {
                            subsetCount++;
                            break;
                        }
                    }
                }
                
                // assign matrix
                matrixAB[x][y] = subsetCount;
                
                // hitung total node di column x dan di row y
                columnCounter[x] += subsetCount;
                rowCounter[y] += subsetCount;
            }
        }
        
        // hitung pembilang
        double pembilang = 0;
        for(int x = 0; x < matrixAB.length; x++)
        {    
            for(int y = 0; y < matrixAB[x].length; y++)
            {
                pembilang += matrixAB[x][y] * Math.log10((matrixAB[x][y] * n) / (columnCounter[x] * rowCounter[y]));
            }
        }
        pembilang *= -2;
        
        // hitung penyebut 1
        double penyebut1 = 0;
        for(int x = 0; x < columnCounter.length; x++)
        {
            double test = Math.log10(columnCounter[x]/n);
            penyebut1 += columnCounter[x] * Math.log10(columnCounter[x] / n);
        }
        
        // hitung penyebut2
        double penyebut2 = 0;
        for(int x = 0; x < rowCounter.length; x++)
        {
            penyebut2 += rowCounter[x] * Math.log10(rowCounter[x] / n);
        }
        
        return pembilang / (penyebut1 + penyebut2);
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import core.Cluster;
import core.Graph;
import core.Node;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;
import util.Debug;

/**
 *
 * @author Isjhar-pc
 */
public class DynamicNetTest {
    
    public DynamicNetTest() {
    }

    @Ignore
    public void overlapTest()
    {
        // Pendefinisian graph 1
        Graph graph = new Graph();
        String node1 = "1";
        String node2 = "2";
        String node3 = "3";
        String node4 = "4";
        graph.addNode(node2);
        graph.addNode(node1);
        graph.addNode(node3);
        graph.addNode(node4);
        
        graph.connectNodes(node1, node1, 20);
        graph.connectNodes(node1, node2, 10);
        graph.connectNodes(node2, node3, 20);
        graph.connectNodes(node3, node4, 30);
        graph.connectNodes(node1, node4, 40);
        graph.connectNodes(node1, node3, 50);
        graph.connectNodes(node2, node4, 60);
        
        Cluster cluster1 = new Cluster();
        cluster1.addMember(graph.getNodes().get(node1));
        cluster1.addMember(graph.getNodes().get(node2));
        
        Cluster cluster2 = new Cluster();
        cluster2.addMember(graph.getNodes().get(node3));
        
        Cluster cluster3 = new Cluster();
        cluster3.addMember(graph.getNodes().get(node4));
       
        graph.getClusters().add(cluster1);
        graph.getClusters().add(cluster2);
        graph.getClusters().add(cluster3);
        
        graph.mergeClusters();
        graph.initClusters();
        
        
        // Pendefinisian graph 2
        Graph graph2 = new Graph();
        String node5 = "5";
        graph.addNode(node2);
        graph.addNode(node1);
        graph.addNode(node5);
        graph.addNode(node4);
        
        graph.connectNodes(node1, node1, 20);
        graph.connectNodes(node1, node2, 10);
        graph.connectNodes(node2, node5, 20);
        graph.connectNodes(node5, node4, 30);
        graph.connectNodes(node1, node4, 40);
        graph.connectNodes(node1, node5, 50);
        graph.connectNodes(node2, node4, 60);
        
        Cluster cluster4 = new Cluster();
        cluster4.addMember(graph.getNodes().get(node1));
        cluster4.addMember(graph.getNodes().get(node2));
        cluster4.addMember(graph.getNodes().get(node5));
        
        Cluster cluster5 = new Cluster();
        cluster5.addMember(graph.getNodes().get(node4));
       
        graph2.getClusters().add(cluster4);
        graph2.getClusters().add(cluster5);
        
        graph.mergeClusters();
        graph.initClusters();
    
        
        DynamicNet dynamicNet = new DynamicNet();
        assertEquals(0, dynamicNet.overlap(graph.getClusters().get(0), graph2.getClusters().get(0)), 0);
        assertEquals(1, dynamicNet.overlap(graph.getClusters().get(2), graph2.getClusters().get(0)), 0);
    }
    
    @Ignore
    public void nmiTest()
    {
        String[] nodeListA = new String[] {"A", "B", "C"};
        String[] nodeListB = new String[] {"A", "B", "C"};
        
        ArrayList<String[]> communityA = new ArrayList<>();
        communityA.add(nodeListA);
        
        ArrayList<String[]> communityB = new ArrayList<>();
        communityB.add(nodeListB);
        
        DynamicNet dynamicNet = new DynamicNet();
        
        assertEquals(1, dynamicNet.NMI(communityA, communityB), 0);
    }
    
    @Ignore
    public void matchTest()
    {
        Node node1 = new Node("1");
        Node node2 = new Node("2");
        
        Cluster cluster1 = new Cluster();
        cluster1.addMember(node1);
        cluster1.addMember(node2);
        
        node1 = new Node("1");
        node2 = new Node("6");
        Node node3 = new Node("3");
        Node node4 = new Node("4");
        Node node5 = new Node("5");
        
        Cluster cluster2 = new Cluster();
        cluster2.addMember(node1);
        cluster2.addMember(node2);
        cluster2.addMember(node3);
        
        Cluster cluster3 = new Cluster();
        cluster3.addMember(node4);
        cluster3.addMember(node5);
        
        ArrayList<Cluster> communityTPlus1List = new ArrayList<>();
        communityTPlus1List.add(cluster2);
        communityTPlus1List.add(cluster3);
        
        DynamicNet dynamicNet = new DynamicNet();
        Cluster matchResult = dynamicNet.match(cluster1, communityTPlus1List, 0.4);
        assertNotNull(matchResult);
    }
    
    @Ignore
    public void surviveTest()
    {
        Node node1 = new Node("1");
        Node node2 = new Node("2");
        Node node3 = new Node("3");
        Node node4 = new Node("4");
        Node node5 = new Node("5");
        Node node6 = new Node("6");
        
        Cluster cluster1 = new Cluster();
        cluster1.addMember(node1);
        cluster1.addMember(node2);
        cluster1.addMember(node3);
        
        Cluster cluster2 = new Cluster();
        cluster2.addMember(node4);
        cluster2.addMember(node5);
        cluster2.addMember(node6);
        
        ArrayList<Cluster> communityT = new ArrayList<>();
        communityT.add(cluster1);
        communityT.add(cluster2);
        
        node1 = new Node("1");
        node2 = new Node("2");
        node3 = new Node("4");
        node4 = new Node("6");
        node5 = new Node("7");
        node6 = new Node("8");
        
        Cluster cluster3 = new Cluster();
        cluster3.addMember(node1);
        cluster3.addMember(node2);
        cluster3.addMember(node3);
        
        Cluster cluster4 = new Cluster();
        cluster4.addMember(node4);
        cluster4.addMember(node5);
        cluster4.addMember(node6);
        
        ArrayList<Cluster> communityTPlus1 = new ArrayList<>();
        communityTPlus1.add(cluster3);
        communityTPlus1.add(cluster4);
        
        DynamicNet dynamicNet = new DynamicNet();
        assertEquals(cluster3, dynamicNet.survives(cluster1, communityT, communityTPlus1, 0.5));
        assertEquals(null, dynamicNet.survives(cluster2, communityT, communityTPlus1, 0.5));
    }
    
    @Ignore
    public void absorbedTest()
    {
        Node node1 = new Node("1");
        Node node2 = new Node("2");
        Node node3 = new Node("3");
        Node node4 = new Node("4");
        Node node5 = new Node("5");
        Node node6 = new Node("6");
        
        Cluster cluster1 = new Cluster();
        cluster1.addMember(node1);
        cluster1.addMember(node2);
        cluster1.addMember(node3);
        
        Cluster cluster2 = new Cluster();
        cluster2.addMember(node4);
        cluster2.addMember(node5);
        cluster2.addMember(node6);
        
        ArrayList<Cluster> communityT = new ArrayList<>();
        communityT.add(cluster1);
        communityT.add(cluster2);
        
        node1 = new Node("1");
        node2 = new Node("2");
        node3 = new Node("4");
        node4 = new Node("5");
        node5 = new Node("7");
        node6 = new Node("8");
        
        Cluster cluster3 = new Cluster();
        cluster3.addMember(node1);
        cluster3.addMember(node2);
        cluster3.addMember(node3);
        cluster3.addMember(node4);
        
        Cluster cluster4 = new Cluster();
        cluster4.addMember(node5);
        cluster4.addMember(node6);
        
        ArrayList<Cluster> communityTPlus1 = new ArrayList<>();
        communityTPlus1.add(cluster3);
        communityTPlus1.add(cluster4);
        
        DynamicNet dynamicNet = new DynamicNet();
        assertEquals(cluster3, dynamicNet.absorbed(cluster1, communityT, communityTPlus1, 0.5));
        assertEquals(cluster3, dynamicNet.absorbed(cluster2, communityT, communityTPlus1, 0.5));
    }
    
    @Ignore
    public void splitTest()
    {
        Node node1 = new Node("1");
        Node node2 = new Node("2");
        Node node3 = new Node("3");
        Node node4 = new Node("4");
        Node node5 = new Node("5");
        Node node6 = new Node("6");
        Node node7 = new Node("7");
        
        Cluster cluster1 = new Cluster();
        cluster1.addMember(node1);
        cluster1.addMember(node2);
        cluster1.addMember(node3);
        cluster1.addMember(node4);
        cluster1.addMember(node5);
        cluster1.addMember(node6);
        cluster1.addMember(node7);
        
        node1 = new Node("1");
        node2 = new Node("2");
        node3 = new Node("3");
        node4 = new Node("4");
        node5 = new Node("5");
        node6 = new Node("6");
        
        Cluster cluster2 = new Cluster();
        cluster2.addMember(node1);
        cluster2.addMember(node2);
        cluster2.addMember(node3);
        cluster2.addMember(node4);
        
        Cluster cluster3 = new Cluster();
        cluster3.addMember(node5);
        cluster3.addMember(node6);
        cluster3.addMember(node7);
        
        ArrayList<Cluster> communityTPlus1 = new ArrayList<>();
        communityTPlus1.add(cluster2);
        communityTPlus1.add(cluster3);
        
        DynamicNet dynamicNet = new DynamicNet();
        ArrayList<Cluster> splitResult = dynamicNet.split(cluster1, communityTPlus1, 0.4);
        assertNotNull(splitResult);
        if(splitResult != null)
             assertEquals(1, splitResult.size());
    }
    
    @Test
    public void dynamictNetTest()
    {
        // definisikan graph pada 2016-01
        Graph graph1 = new Graph("2016-01");
        graph1.addNode("1");
        graph1.addNode("2");
        graph1.addNode("3");
        graph1.addNode("4");
        graph1.addNode("5");
        
        graph1.connectNodes("1", "2", 1);
        graph1.connectNodes("2", "3", 1);
        graph1.connectNodes("1", "3", 1);
        graph1.connectNodes("4", "5", 1);
        
        Cluster cluster11 = new Cluster();
        cluster11.addMember(graph1.getNodes().get("1"));
        cluster11.addMember(graph1.getNodes().get("2"));
        cluster11.addMember(graph1.getNodes().get("3"));
        graph1.getClusters().add(cluster11);
        
        Cluster cluster12 = new Cluster();
        cluster12.addMember(graph1.getNodes().get("4"));
        cluster12.addMember(graph1.getNodes().get("5"));
        graph1.getClusters().add(cluster12);
        
        
        // definisikan graph pada 2016-02
        Graph graph2 = new Graph("2016-02");
        graph2.addNode("1");
        graph2.addNode("2");
        graph2.addNode("3");
        graph2.addNode("4");
        graph2.addNode("5");
        graph2.addNode("6");
        
        graph2.connectNodes("1", "2", 1);
        graph2.connectNodes("4", "3", 1);
        graph2.connectNodes("3", "5", 1);
        
        Cluster cluster21 = new Cluster();
        cluster21.addMember(graph2.getNodes().get("1"));
        cluster21.addMember(graph2.getNodes().get("2"));
        graph2.getClusters().add(cluster21);
        
        Cluster cluster22 = new Cluster();
        cluster22.addMember(graph2.getNodes().get("3"));
        cluster22.addMember(graph2.getNodes().get("4"));
        cluster22.addMember(graph2.getNodes().get("5"));
        graph2.getClusters().add(cluster22);
        
        Cluster cluster23 = new Cluster();
        cluster23.addMember(graph2.getNodes().get("6"));
        graph2.getClusters().add(cluster23);
        
        // definisikan graph pada 2016-02
        Graph graph3 = new Graph("2016-03");
        graph3.addNode("1");
        graph3.addNode("2");
        
        graph3.connectNodes("1", "2", 1);
        
        Cluster cluster31 = new Cluster();
        cluster31.addMember(graph3.getNodes().get("1"));
        cluster31.addMember(graph3.getNodes().get("2"));
        graph3.getClusters().add(cluster31);
        
        ArrayList<Graph> graphs = new ArrayList<>();
        graphs.add(graph1);
        graphs.add(graph2);
        graphs.add(graph3);
        
        
        DynamicNet dynamicNet = new DynamicNet();
        try {
            ArrayList<ArrayList<Cluster>> lifeTimes = dynamicNet.dynamicNet(graphs, 0.5, false);
            
            for(ArrayList<Cluster> lifeTime : lifeTimes)
            {
                int i = 0;
                String temp = "";
                for(Cluster cluster : lifeTime)
                {
                    if(i == 0)
                    {
                        temp += "[" + cluster.getId() + "] ";
                    }
                    else
                    {
                        temp += "-> [" + cluster.getId() + "]";
                    }
                    i++;
                }
                Debug.logFormat("Life Time : %s", temp);
            }
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(DynamicNetTest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DynamicNetTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
}
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package core;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Isjhar-pc
 */
public class ClusterTest {
    
    public ClusterTest() {
    }

    /**
     * Test of getSigmaIn method, of class Cluster.
     */
    @Test
    public void testGetSigmaInAndTotal() 
    {
        Graph graph = new Graph();
        String node1 = "1";
        String node2 = "2";
        String node3 = "3";
        String node4 = "4";
        graph.addNode(node2);
        graph.addNode(node1);
        graph.addNode(node3);
        graph.addNode(node4);
        
        graph.connectNodes(node1, node1, 100);
        graph.connectNodes(node1, node2, 100);
        graph.connectNodes(node2, node3, 100);
        graph.connectNodes(node3, node4, 100);
        graph.connectNodes(node1, node4, 100);
        graph.connectNodes(node1, node3, 100);
        graph.connectNodes(node2, node4, 100);
        
        Cluster c1 = new Cluster();
        c1.addMember(graph.getNodes().get(node1));
        c1.addMember(graph.getNodes().get(node2));
        c1.addMember(graph.getNodes().get(node3));
        
        Cluster c2 = new Cluster();
        c2.addMember(graph.getNodes().get(node4));
        
        assertEquals(400, c1.getSigmaIn(), 0);
        assertEquals(0, c2.getSigmaIn(), 0);
        assertEquals(700, c1.getSigmaTot(), 0);
        assertEquals(300, c2.getSigmaTot(), 0);
       
    }
}
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package core;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.junit.Test;
import static org.junit.Assert.*;
import util.Debug;

/**
 *
 * @author Isjhar-pc
 */
public class GraphTest {
    
    public GraphTest() {
    }


    @Test
    public void testAddNode()
    {
        Graph graph = new Graph();
        String node1 = "1";
        String node2 = "1";
        graph.addNode(node2);
        graph.addNode(node1);
        
        assertEquals(1, graph.getNodes().size());
    }
    
    /**
     * Test of ConnectNodes method, of class Graph.
     */
    @Test
    public void testConnectNodes_3args_1() 
    {
        Graph graph = new Graph();
        String node1 = "1";
        String node2 = "2";
        graph.addNode(node2);
        graph.addNode(node1);
        
        graph.connectNodes(node1, node2, 100);
        graph.connectNodes(node1, node2, 100);
        
        assertEquals(graph.getNodes().get(node1).getNeighbour().size(), 1);
        assertEquals(graph.getNodes().get(node1).getCallDurationToNode(node2), 200, 0);
    }
    
    @Test
    public void testGetM()
    {
        Graph graph = new Graph();
        String node1 = "1";
        String node2 = "2";
        String node3 = "3";
        String node4 = "4";
        graph.addNode(node2);
        graph.addNode(node1);
        graph.addNode(node3);
        graph.addNode(node4);
        
        graph.connectNodes(node1, node1, 100);
        graph.connectNodes(node1, node2, 100);
        graph.connectNodes(node2, node2, 100);
        graph.connectNodes(node2, node3, 100);
        graph.connectNodes(node3, node4, 100);
        graph.connectNodes(node1, node4, 100);
        graph.connectNodes(node1, node3, 100);
        graph.connectNodes(node2, node4, 100);
        
        assertEquals(800, graph.getM(), 0);
    }
    
    @Test
    public void testDeltaQ()
    {
        assertEquals(0, Graph.getDeltaQ(10, 10, 10, 10, 10), 0);
    }
    
    @Test
    public void testBlondelAlgorithm()
    {
        Graph graph = new Graph();
        String node1 = "1";
        String node2 = "2";
        String node3 = "3";
        String node4 = "4";
        graph.addNode(node2);
        graph.addNode(node1);
        graph.addNode(node3);
        graph.addNode(node4);
        
        graph.connectNodes(node1, node2, 10);
        graph.connectNodes(node2, node3, 20);
        graph.connectNodes(node3, node4, 30);
        graph.connectNodes(node1, node4, 40);
        graph.connectNodes(node1, node3, 50);
        graph.connectNodes(node2, node4, 60);
        
        graph.clustering();
        assertEquals(4, graph.getClusterSize());
    }
    
    @Test
    public void testMergeClusters()
    {
        Graph graph = new Graph();
        String node1 = "1";
        String node2 = "2";
        String node3 = "3";
        String node4 = "4";
        graph.addNode(node2);
        graph.addNode(node1);
        graph.addNode(node3);
        graph.addNode(node4);
        
        graph.connectNodes(node1, node1, 20);
        graph.connectNodes(node1, node2, 10);
        graph.connectNodes(node2, node3, 20);
        graph.connectNodes(node3, node4, 30);
        graph.connectNodes(node1, node4, 40);
        graph.connectNodes(node1, node3, 50);
        graph.connectNodes(node2, node4, 60);
        
        Cluster cluster1 = new Cluster();
        cluster1.addMember(graph.getNodes().get(node1));
        cluster1.addMember(graph.getNodes().get(node2));
        
        Cluster cluster2 = new Cluster();
        cluster2.addMember(graph.getNodes().get(node3));
        
        Cluster cluster3 = new Cluster();
        cluster3.addMember(graph.getNodes().get(node4));
        
        
        graph.getClusters().add(cluster3);
        graph.getClusters().add(cluster2);
        graph.getClusters().add(cluster1);
        
        
        graph.mergeClusters();
        assertEquals(3, graph.getClusterSize());
    }
    
    @Test
    public void testSaveToFile()
    {
        String pathFile = "data/DataDummy.xlsx";
        String testId = "12-2014";
        Graph testGraph = null;
        try {
            ArrayList<Graph> graphs = DataReader.readData(pathFile);
            for(Graph entry : graphs)
            {
                entry.SaveToFile();
                Debug.log("\n");
            }
        } catch (FileNotFoundException ex) {
            Debug.log(ex.getMessage());
        } catch (IOException ex) {
             Debug.log(ex.getMessage());
        }
    }
}